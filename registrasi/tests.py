from django.test import TestCase, Client
from .forms import RegisterForm
from django.contrib.auth.models import User, auth
from django.contrib.auth import authenticate, login

# Create your tests here.

class Registrasi(TestCase):
    def test_url_login_ada(self):
        response = self.client.get('/login/')
        self.assertEqual(response.status_code, 200)

    def test_url_logout_ada(self):
        response = self.client.get('/logout/')
        self.assertEqual(response.status_code, 302)
    
    def test_url_registrasi_ada(self):
        response = self.client.get('/registrasi/')
        self.assertEqual(response.status_code, 200)
    
    def test_template_login_ada(self):
        response = self.client.get('/login/')
        self.assertTemplateUsed(response,'registration/login.html')
    
    def test_template_regitrasi_ada(self):
        response = self.client.get('/registrasi/')
        self.assertTemplateUsed(response,'registrasi/sign_up.html')
    
    def test_form_validation_for_blank_items(self):
        form = RegisterForm(data={"username": "", "email": "", "password1": "", "password2": ""})
        self.assertFalse(form.is_valid())
    
    def test_login_user_berhasil(self):
        user = User.objects.create(username='username')
        user.set_password('123')
        user.save()
        logged_in = Client().login(username='username', password='123')
        self.assertTrue(logged_in)