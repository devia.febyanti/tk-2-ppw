from django.test import LiveServerTestCase, TestCase, tag
from django.urls import reverse, resolve
from selenium import webdriver
from .models import jawabanTest, Comment, Pasien
from .forms import CommentForm, tambah_pasien, hapus_pasien, jawabanTest
from django.contrib.auth.models import User, auth
from django.contrib.auth import authenticate, login
from django.test import Client
from . import views

@tag('functional')
class FunctionalTestCase(LiveServerTestCase):
    """Base class for functional test cases with selenium."""

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        # Change to another webdriver if desired (and update CI accordingly).
        options = webdriver.chrome.options.Options()
        # These options are needed for CI with Chromium.
        options.headless = True  # Disable GUI.
        options.add_argument('--no-sandbox')
        options.add_argument('--disable-dev-shm-usage')
        cls.selenium = webdriver.Chrome(options=options)

    @classmethod
    def tearDownClass(cls):
        cls.selenium.quit()
        super().tearDownClass()


class MainTestCase(TestCase):
    def test_root_url_status_200(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)
        # You can also use path names instead of explicit paths.
        response = self.client.get(reverse('main:home'))
        self.assertEqual(response.status_code, 200)


class MainFunctionalTestCase(FunctionalTestCase):
    def test_root_url_exists(self):
        self.selenium.get(f'{self.live_server_url}/')
        html = self.selenium.find_element_by_tag_name('html')
        self.assertNotIn('not found', html.text.lower())
        self.assertNotIn('error', html.text.lower())


class B02_TK_1(TestCase):
    def test_url_slash_ada(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)
    
    def test_url_dan_template_pencegahan_ada(self):
        response = self.client.get('/pencegahan-covid-19')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response,'main/pencegahan.html')

    def test_url_dan_template_rs_ada(self):
        response = self.client.get('/rumah-sakit-rujukan')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response,'main/rs.html')

    def test_url_pencarian_ada(self):
        response = self.client.get('/pencarian/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response,'main/pencarian.html')

    def test_url_dan_template_test_ada(self):
        response = self.client.get('/test')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response,'main/covidtest.html')

    def test_url_add_comment_ada(self):
        response = self.client.get('/add_comment')
        self.assertEqual(response.status_code, 302)

    def test_url_datas_ada(self):
        response = self.client.get('/datas/')
        self.assertEqual(response.status_code, 200)

    def test_text_comment_dan_submit_ada(self):
        user = User.objects.create(username='testuser')
        user.set_password('12345')
        user.save()
        c = Client()
        logged_in = c.login(username='testuser', password='12345')
        response = c.get('/pencegahan-covid-19')
        kembalian = response.content.decode('utf8')
        self.assertIn("Tinggalkan komentar",kembalian)
        self.assertIn("Email:",kembalian)
        self.assertIn("Nama:",kembalian)
        self.assertIn("Komentar:",kembalian)
        self.assertIn("Submit",kembalian)
    
    def test_url_pasien_ada(self):
        response = self.client.get('/pasien')
        self.assertEqual(response.status_code, 200)

    def test_template_pasien_ada(self):
        response = self.client.get('/pasien')
        self.assertTemplateUsed(response,'main/pasien.html')

    def test_url_dan_template_tambah_pasien_ada(self):
        response = self.client.get('/tambah-pasien')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response,'main/tambah_pasien.html')
    
    def test_url_dan_template_hapus_pasien_ada(self):
        response = self.client.get('/hapus-pasien')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response,'main/tambah_pasien.html')

    def test_text_daftar_pasien_ada(self):
        response = self.client.get('/pasien')
        html_kembalian = response.content.decode('utf8')
        self.assertIn("Daftar Pasien", html_kembalian)
    
    def test_model_pasien(self):
        Pasien.objects.create(nama_pasien="nama", umur="20", gender="P", pekerjaan="pekerjaan", alamat="alamat", berat_badan="50", tinggi_badan="150", keluhan="keluhan", email="email")
        hitung_berapa_pasien = Pasien.objects.all().count()
        self.assertEquals(hitung_berapa_pasien, 1)
    
    def test_model_comment(self):
        Comment.objects.create(email="email", nama="nama", komentar="komentar", created="created")
        hitung_berapa_komen = Comment.objects.all().count()
        self.assertEquals(hitung_berapa_komen, 1)

    def test_model_checker(self):
        jawabanTest.objects.create(question1=True, question2=True, question3=True)
        hitung_berapa_checker = jawabanTest.objects.all().count()
        self.assertEquals(hitung_berapa_checker, 1)

    def test_url_cari_buku_ada(self):
        response = self.client.get('/cari-buku')
        self.assertEqual(response.status_code, 200)

    def test_template_cari_buku_ada(self):
        response = self.client.get('/cari-buku')
        self.assertTemplateUsed(response,'main/cari_buku.html')
    
    def test_hasil_cari_buku(self):
        kembalian = resolve(reverse('main:rekomendasi-buku'))
        self.assertTrue(kembalian.func, views.hasil_cari_buku)