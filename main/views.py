from django.shortcuts import render, redirect
from django.views.generic import TemplateView
from django.http import HttpResponseRedirect
from .models import jawabanTest, Comment, Pasien
from .forms import CreateCovidTest, CommentForm, tambah_pasien, hapus_pasien
from django.http import JsonResponse
import requests #Note: kalau mau pake ini, kita harus "pip install requests" dulu di cmd venv kita
import json


def home(request):
    return render(request, 'main/home.html')

def rs_rujukan(request):
    return render(request, 'main/rs.html')

def pencegahan(request):
	persons = Comment.objects.all()
	response = {
		'persons':persons,
		'CommentForm':CommentForm
	}
	return render(request, 'main/pencegahan.html',response)

def covidtest(request):
	response = {'covidtest': CreateCovidTest()}
	return render(request,'main/covidtest.html', response)

def hasiltest(request):
	if(request.method == 'POST'):
		form = CreateCovidTest(request.POST)
		if form.is_valid():
			question1 = form.cleaned_data['question1']
			
			if(question1 == True):
				return render(request, 'main/positif.html')
			else:
				return render(request, 'main/negatif.html')

def add_comment(request):
	form = CommentForm(request.POST or None)
	if (form.is_valid and request.method == 'POST') :
		form.save()
		return HttpResponseRedirect('/pencegahan-covid-19')
	else:
		return HttpResponseRedirect('/')

def deletecomment(request,nomor):
	Comment.objects.filter(id=nomor).delete()
	return HttpResponseRedirect('/pencegahan-covid-19')

class add_pasien(TemplateView):
    namaTemplate = "main/tambah_pasien.html"
    def get(self, req):
        daftar_pasien = [(nama_pasien.nama_pasien, nama_pasien.nama_pasien) for nama_pasien in Pasien.objects.all()]
        form = tambah_pasien()
        form_delete = hapus_pasien(daftar_pasien)
        return render(req, self.namaTemplate, {"form": form, "delete": form_delete})
    
    def post(self, req):
        form = tambah_pasien(req.POST)
        if form.is_valid():
            form.save()
            return redirect("main:pasien")
        return render(req, self.namaTemplate, {"form": form})

class delete_pasien(TemplateView):
    namaTemplate = "main/tambah_pasien.html"
    def get(self, req):
        daftar_pasien = [(nama_pasien.nama_pasien, nama_pasien.nama_pasien) for nama_pasien in Pasien.objects.all()]
        form = tambah_pasien()
        form_delete = hapus_pasien(daftar_pasien)
        return render(req, self.namaTemplate, {"form": form, "delete": form_delete})
    
    def post(self,req):
        form = tambah_pasien()
        daftar_pasien = [(nama_pasien.nama_pasien, nama_pasien.nama_pasien) for nama_pasien in Pasien.objects.all()]
        form_delete = hapus_pasien(daftar_pasien, req.POST)
        if form_delete.is_valid():
            print(form_delete)
            Pasien.objects.get(nama_pasien=req.POST.get("nama")).delete()
            return redirect("main:pasien")
        return render(req, self.namaTemplate, {"form": form, "delete": form_delete})

def get_pasien(req):
    nama = Pasien.objects.all()
    return render(req, "main/pasien.html", {'nama': nama})

def get_daftar_pasien(req, name):
    nama_pasien = Pasien.objects.get(pk=name)
    return render(req, "main/data_pasien.html", {"nama_pasien": nama_pasien})

def pencarian(request):
    try :
        request.session['has_search'] = request.session['has_search'] +1
    except :
        request.session['has_search'] = 1
    response = {
        "jumlah_pencarian":request.session['has_search']
    }
    return render(request, 'main/pencarian.html',response)

def datas(request):
    url = "https://services5.arcgis.com/VS6HdKS0VfIhv8Ct/arcgis/rest/services/COVID19_Indonesia_per_Provinsi/FeatureServer/0/query?where=1%3D1&outFields=*&outSR=4326&f=json"
    ret = requests.get(url)
    data = json.loads(ret.content)
    return JsonResponse(data, safe = False)

def cari_buku(request):
    try:
        request.session['has_search'] = request.session['has_search'] + 1
    except:
        request.session['has_search'] = 1
    
    response = {
        "count":request.session['has_search']
    }
    return render(request, 'main/cari_buku.html',response)

def hasil_cari_buku(request):
    url = 'https://www.googleapis.com/books/v1/volumes?q=' + request.GET['q']
    hasil = requests.get(url)
    data = json.loads(hasil.content)
    return JsonResponse(data, safe = False)