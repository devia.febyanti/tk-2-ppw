# Generated by Django 3.1.3 on 2020-11-24 05:08

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0009_auto_20201124_1201'),
    ]

    operations = [
        migrations.AlterField(
            model_name='pasien',
            name='gender',
            field=models.CharField(choices=[('Perempuan', 'P'), ('Laki-laki', 'L')], default=None, max_length=10),
        ),
    ]
