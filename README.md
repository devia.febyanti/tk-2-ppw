# Kelompok B02

[![Test and Deploy][actions-badge]][commits-gh]
[![pipeline status][pipeline-badge]][commits-gl]
[![coverage report][coverage-badge]][commits-gl]


## Daftar isi

- [Anggota Kelompok](#anggota-kelompok)
- [Link Herokuapp](#link-herokuapp)
- [Cerita Aplikasi beserta Kebermanfaatannya](#cerita-aplikasi-beserta-kebermanfaatannya)
- [Daftar Fitur yang diimplementasikan](#daftar-fitur-yang-diimplementasikan)


## Anggota Kelompok

1. 1806196806 - Ahmad Dzikrul Fikri
2. 1806204884 - Ariq Rahmatullah
3. 1806136706 - Devia Febyanti
4. 1806205792 - Zovier Zharvan Filhaq


## Link Herokuapp

https://covid-checker-ppw.herokuapp.com/


## Cerita Aplikasi beserta Kebermanfaatannya

Web ini akan dijadikan aplikasi covid checker, yaitu untuk mengecek secara online apakah seseorang memenuhi gejala positif Covid-19 atau tidak. Pada home page akan diberikan informasi mengenai apa itu Covid-19 kemudian akan ada tombol "Mulai Tes" yang mengarah ke halaman form atau checkbox dimana pengguna akan menginput beberapa data pada form tersebut, misalnya keluhan-keluhan mengenai kesehatan yang sedang dialami. Setelah pengguna men-submit maka akan diberikan informasi apakah pengguna tersebut memiliki gejala positif Covid-19 atau tidak memiliki gejala positif Covid-19. Jika pengguna memiliki gejala positif Covid-19 maka akan diberikan beberapa saran yang dapat dilakukan oleh pengguna. Selain itu pada website akan dibuat halaman khusus berisi list rumah sakit rujukan untuk pasien Covid-19 dan ada halaman tambahan berisi tata cara pencegahan penularan Covid-19.

Website ini bertujuan untuk membantu seluruh pengguna untuk mengetahui apakah ia memiliki gejala positif Covid-19 atau tidak serta memberikan arahan atau saran kepada pengguna yang memiliki gejala positif Covid-19 tersebut agar penularan atau angka positif dan kematian pasien Covid-19 dapat diminimalisir.

Pengguna yang memiliki gejala positif Covid-19 dapat melakukan pendaftaran Rapid Test, dan harus register dan log in terlebih dahulu. Setelah pengguna tersebut selesai Rapid Test maka data Pengguna tersebut di dalam list Rapid Test dapat di hapus.

## Daftar Fitur yang diimplementasikan

- Mobile responsive, untuk membuat website yang responsive yaitu website akan menyesuaikan tampilan di segala macam device.
- Fitur covid checker yang menyediakan form berisi checkbox mengenai gejala-gejala yang sedang di alami pengguna
- Home page berisikan informasi mengenai apa itu Covid-19 dan button untuk memulai tes dan juga rapid test
- Fitur tabel rumah sakit rujukan
- Fitur tata cara pencegahan penularan Covid-19
- Fitur hasil pengecekan yang menyatakan memiliki gejala positif Covid-19 atau negatif 
- Fitur saran yang dapat dilakukan jika memiliki gejala positif Covid-19
- Fitur komentar yang terdapat pada halaman Tata Cara Pencegahan Covid-19
- Fitur register dan log in untuk autentikasi
- Fitur hiburan untuk mengisi waktu luang di rumah saja seperti rekomendasi buku
- Fitur data positif Covid-19 pada setiap provinsi
- Fitur accordion di halaman Tata Cara Pencegahan Covid-19
- Fitur penggantian warna tabel (CSS)

[actions-badge]: https://github.com/laymonage/django-template-heroku/workflows/Test%20and%20Deploy/badge.svg
[commits-gh]: https://github.com/laymonage/django-template-heroku/commits/master
[pipeline-badge]: https://gitlab.com/devia.febyanti/tk-2-ppw/badges/master/pipeline.svg
[coverage-badge]: https://gitlab.com/devia.febyanti/tk-2-ppw/badges/master/coverage.svg
[commits-gl]: https://gitlab.com/devia.febyanti/tk-2-ppw/-/commits/master